#|
  This file is a part of ansi-common-lisp project.
|#

(defsystem "ansi-common-lisp"
  :version "0.1.0"
  :author "Michael Reis"
  :license "Public"
  :depends-on ("mtreis-scripts")
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "ch2")
                 (:file "ch3")
                 (:file "ch4")
                 (:file "ch5")
                 (:file "ch6")
                 (:file "ch7")
                 (:file "ch8")
                 (:file "ch9"))))
  :description "Answers and examples from ANSI Common Lisp by Paul Graham"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "ansi-common-lisp-test"))))
