;;;; Chapter 6 of ANSI Common Lisp
(in-package ch6)

(defun single? (lst)
  (and (consp lst)
       (null (cdr lst))))

(defun append1 (lst obj)
  (append lst (list obj)))

(defun mpa-int (fn n)
  (let ((acc nil))
    (dotimes (i n)
      (push (funcall fn i) acc))
    (nreverse acc)))

(defun filter (fn lst)
  (let ((acc nil))
    (dolist (x lst)
      (let ((val (funcall fn x)))
        (when val (push val acc))))
    (nreverse acc)))

(defun most (fn lst)
  (if (null lst)
      (values nil nil)
      (let* ((wins (car lst))
             (max (funcall fn wins)))
        (dolist (obj (cdr lst))
          (let ((score (funcall fn obj)))
            (when (> score max)
              (setf wins obj
                    max score))))
        (values wins max))))

(defun compose (&rest fns)
  (destructuring-bind (fn1 . rest) (reverse fns)
    #'(lambda (&rest args)
        (reduce #'(lambda (v f) (funcall f v))
                rest
                :initial-value (apply fn1 args)))))

(defun disjoin (fn &rest fns)
  (if (null fns)
      fn
      (let ((disj (apply #'disjoin fns)))
        #'(lambda (&rest args)
            (or (apply fn args)
                (apply disj args))))))

(defun conjoin (fn &rest fns)
  (if (null fns)
      fn
      (let ((conj (apply #'conjoin fns)))
        #'(lambda (&rest args)
            (and (apply fn args)
                 (apply conj args))))))

(defun curry (fn &rest args)
  #'(lambda (&rest args2)
      (apply fn (append args args2))))

(defun rcurry (fn &rest args)
  #'(lambda (&rest args2)
      (apply fn (append args2 args))))

(defun always (x)
  #'(lambda (&rest args)
      (declare (ignore args))
      x))


;;;Exercises

;; 1 Add test and start keys to tokens

(defun tokens (string &key (test #'constituent) (start 0))
  "Break apart a string into tokens using a test function to separate them."
  (let ((pos1 (position-if test string :start start)))
    (if pos1
        (let ((pos2 (position-if #'(lambda (char)
                                     (not (funcall test char)))
                                 string :start pos1)))
          (cons (subseq string pos1 pos2)
                (if pos2
                    (tokens string :test test :start pos2)
                    nil)))
        nil)))


;; 2 Add key, test, start, and end keys to bin-search
;;I don't see how key can be used here, going to skip it.

(defun bin-search (obj vec &key (test #'eql) (start 0) (end (- (length vec) 1)))
  "Use finder to start a binary search in a sorted vec for the obj."
  (let ((len (length vec)))
    (and (not (zerop len))
         (finder obj vec :start start :end end :test test))))


(defun finder (obj vec &key (start 0) (end (- (length vec) 1)) (test #'eql))
  "Take an obj and a range within a vec and use binary search to see if the obj exists in that range."
  (format t "~a~%" (subseq vec start (+ end 1)))
  (let ((range (- end start)))
    (if (zerop range)
        (if (funcall test obj (aref vec start))
            obj
            nil)
        (let* ((mid (+ start (round (/ range 2))))
               (obj2 (aref vec mid)))
          (if (< obj obj2)
              (finder obj vec :start start :end (- mid 1) :test test)
              (if (> obj obj2)
                  (finder obj vec :start (+ mid 1) :end end :test test)
                  obj))))))


;; 3 Return the number of arguements passed in
(defun count-args (&rest args)
  (length args))


;; 4 Modify most to return the two highest scoring variables

(defun most-and-mostest (fn lst)
  (if (null lst)
      (values nil nil)
      (let* ((wins (car lst))
             (2nd nil)
             (max (funcall fn wins)))
        (dolist (obj (cdr lst))
          (let ((score (funcall fn obj)))
            (when (> score max)
              (setf 2nd wins)
              (setf wins obj
                    max score))))
        (values wins 2nd))))


;; 5 Define remove-if in terms of filter

(defun my-remove-if (function list)
  (loop for entry in list collecting (filter function list)))


;; 6 Define a function that takes one arguement, a number, and returns the greatest value passed to it so far.

(let (highest)
  (defun highest-passed (number)
    (when (null highest) (setf highest number))
    (when (> number highest)
      (setf highest number))
    highest))

(let (previous)
  (defun greater-than-previous (number)
    (let (result)
      (if (null previous)
          (setf previous number)
          (progn (when (> number previous)
                   (setf result t))
                 (setf previous number)))
      result)))


;; 7 Define a function that wraps anothe function but only calls it if it hasn't been called with that arguement before

(defun expensive (number)
  ;;do something that takes some time
  (let ((result 0))
    (loop for i from 1 to 99999999 do
      (incf result i))
    (print "Expensive Called")
    (mod result number)))

(let ((previous (make-hash-table)))
  (defun frugal (number)
    (let ((old-result (gethash number previous)))
      (if old-result
          old-result
          (setf (gethash number previous) (expensive number))))))


;; 8 Define a function like apply but any number printed before it returns in in octal
(defun apply-in-octal (function arg &rest args)
  (let ((*print-base* 8))
    (apply function arg args)))
