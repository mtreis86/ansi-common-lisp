;;;; Chapter 7 from ANSI Common Lisp

(in-package :ch7)


;;; Ring buffer

(defstruct buf
  vec (start -1) (used -1) (new -1) (end -1))

(defun bref (buf n)
  (svref (buf-vec buf)
         (mod n (length (buf-vec buf)))))

(defun (setf bref) (val buf n)
  (setf (svref (buf-vec buf)
               (mod n (length (buf-vec buf))))
        val))

(defun new-buf (len)
  (make-buf :vec (make-array len)))

(defun buf-insert (x b)
  (setf (bref b (incf (buf-end b))) x))

(defun buf-pop (b)
  (prog1
    (bref b (incf (buf-start b)))
    (setf (buf-used b) (buf-start b)
          (buf-new b) (buf-end b))))

(defun buf-next (b)
  (when (< (buf-used b) (buf-new b))
    (bref b (incf (buf-used b)))))

(defun buf-reset (b)
  (setf (buf-used b) (buf-start b)
        (buf-new b) (buf-end b)))

(defun buf-clear (b)
  (setf (buf-start b) -1 (buf-used b) -1
        (buf-new b) -1 (buf-end b) -1))

(defun buf-flush (b str)
  (do ((i (1+ (buf-used b)) (1+ i)))
      ((> i (buf-end b)))
    (princ (bref b i) str)))


;;; String substitution in files

(defun file-subst (old new file1 file2)
  (with-open-file (in file1 :direction :input)
    (with-open-file (out file2 :direction :output
                               :if-exists :supersede)
      (stream-subst old new in out))))

(defun stream-subst (old new in out)
  (let* ((pos 0)
         (len (length old))
         (buf (new-buf len))
         (from-buf nil))
    (do ((c (read-char in nil :eof)
            (or (setf from-buf (buf-next buf))
                (read-char in nil :eof))))
        ((eql c :eof))
      (cond ((char= c (char old pos))
             (incf pos)
             (cond ((= pos len)
                    (princ new out)
                    (setf pos 0)
                    (buf-clear buf))
                   ((not from-buf)
                    (buf-insert c buf))))
            ((zerop pos)
             (princ c out)
             (when from-buf
               (buf-pop buf)
               (buf-reset buf)))
            (t
             (unless from-buf
               (buf-insert c buf))
             (princ (buf-pop buf) out)
             (buf-reset buf)
             (setf pos 0))))
    (buf-flush buf out)))



;;; Exercises


;; 1 Files to strings

(defun file->strings (path)
  "Take the file at the path and return a list of strings, line by line in the file."
  (let (strings)
    (with-open-file (input path :direction :input)
      (loop for line = (read-line input nil)
        while line do (setf strings (append strings (list line)))))
    strings))


;; 2 Files to expressions

(defun file->exprs (path)
  "Take the file at the path and return a list of expressions."
  (let (exprs)
    (with-open-file (input path :direction :input)
      (loop for expr = (read input nil)
            while expr do (setf exprs (append exprs (list expr)))))
    exprs))


;; 3 Copy file without % comments

(defun copy-without-%-comments (path-in path-out)
  (copy-file-without-comment path-in path-out #\%))

(defun copy-file-without-comment (path-in path-out comment)
  "Copy the file at path-in to path-out skipping lines that begin with comment."
  (with-open-file (input path-in :direction :input)
    (with-open-file (output path-out :direction :output :if-exists :supersede)
      (loop for line = (read-line input nil)
            while line
              unless (first-char line comment) do
                (princ line output)
                (terpri output)))))

(defun first-char (string char)
  "Return true if the first character is char, skipping whitespace."
  (let (result)
    (declare (type string string))
    (loop for index from 0 below (length string) do
      (let ((character (subseq string index (+ index 1))))
        (when (not (indentp character))
          (if (string= character char)
              (setf result t)
              (return)))))
    result))

(defun indentp (char)
  "Is char an indent? Space, tab"
  (or (string= char #\Space)
      (string= char #\Tab)))


;; 4 Tabitulate float array
(defun tabitulate ()
  (let ((array (make-array '(5 5))))
    (loop-over-2d-array array
      do (setf (aref array row col)
               (float (/ 1 (* (+ row 1) (+ col 1))))))
    (tabitulate-array array)))

(defun tabitulate-array (array)
  (loop-over-2d-array array
    collecting (aref array row col) into entries
    finally (format t "~{~9,7,,'0f ~}~%" entries)))


;; 5 Add wildcard #\+ for any character match to stream-subst
;; 6 Add wildcards for digits #\% and alphanumerics #\& to stream-subst

;; Test for direct substitution
(defun test-stream-direct ()
  (with-open-file (input "~/ansi/src/ch7input.txt" :direction :input)
    (with-open-file (output "~/ansi/src/ch7output.txt" :direction :output
                            :if-exists :supersede)
      (princ "this 1 isnt a test" output)
      (terpri output)
      (my-stream-subst " is" " isnt" input output))))

;; Test for wildcard any
(defun test-stream-any ()
  (with-open-file (input "~/ansi/src/ch7input.txt" :direction :input)
    (with-open-file (output "~/ansi/src/ch7output.txt" :direction :output
                            :if-exists :supersede)
      (princ "tzef 1zef a zeft" output)
      (terpri output)
      (my-stream-subst "++s" "zef" input output))))

;; Test for wildcard alphanumeric
(defun test-stream-alpha ()
  (with-open-file (input "~/ansi/src/ch7input.txt" :direction :input)
    (with-open-file (output "~/ansi/src/ch7output.txt" :direction :output
                            :if-exists :supersede)
      (princ "this 4 is 4 test" output)
      (terpri output)
      (my-stream-subst "is $" "is 4" input output))))

;; Test for wildcard number
(defun test-stream-num ()
  (with-open-file (input "~/ansi/src/ch7input.txt" :direction :input)
    (with-open-file (output "~/ansi/src/ch7output.txt" :direction :output
                            :if-exists :supersede)
      (princ "this 4 is a test" output)
      (terpri output)
      (my-stream-subst "is %" "is 4" input output))))

;; Wildcards:   + any   $ alphanumeric   % number
(defun my-stream-subst (old new in out)
  (let* ((pos 0)
         (len (length old))
         (buf (new-buf len))
         (from-buf nil))
    (do ((c (read-char in nil :eof)
            (or (setf from-buf (buf-next buf))
                (read-char in nil :eof))))
        ((eql c :eof))
      (let ((match (match? c (char old pos))))
        (cond ((eql match t)
               (incf pos)
               (cond ((= pos len)            ; Match finished
                      (princ new out)
                      (setf pos 0)
                      (buf-clear buf))
                     ((and (eql match 't)    ; Match
                           (not from-buf))
                      (buf-insert c buf))))
              ((zerop pos)                   ; Not yet matched, push this one to out
               (princ c out)
               (when from-buf
                 (buf-pop buf)
                 (buf-reset buf)))
              (t                             ; Match failed
               (unless from-buf
                 (buf-insert c buf))
               (princ (buf-pop buf) out)
               (buf-reset buf)
               (setf pos 0)))))
    (buf-flush buf out)))

(defun match? (char-in-question char-to-match)
  "Does char-to-match equal char-in-questions? Accounting for wildcards. If wildcard match, return the wildcard."
  (cond ((char= char-in-question char-to-match) t)
        ((char= char-to-match #\+) t)
        ((and (char= char-to-match #\$)
              (alphanumericp char-in-question)) t)
        ((and (char= char-to-match #\%)
              (char-num char-in-question)))
        (t nil)))

(defun char-num (char)
  "Is the character a number?"
  (or (char= char #\0)
      (char= char  #\1)
      (char= char  #\2)
      (char= char  #\3)
      (char= char  #\4)
      (char= char  #\5)
      (char= char  #\6)
      (char= char  #\7)
      (char= char  #\8)
      (char= char  #\9)))
