(in-package :ch8)

;;;; Chapter 8 from ANSI Common Lisp

(defparameter *words* (make-hash-table :size 10000))
(defparameter *iwords* (make-hash-table :size 10000))

(defconstant maxword 100)

(defun read-text (pathname)
  (with-open-file (s pathname :direction :input)
    (let ((buffer (make-string maxword))
          (pos 0))
      (do ((c (read-char s nil :eof)
              (read-char s nil :eof)))
          ((eql c :eof))
        (if (or (alpha-char-p c) (char= c #\'))
            (progn (setf (aref buffer pos) c)
                   (incf pos))
            (progn (unless (zerop pos)
                     (see (intern (string-downcase
                                   (subseq buffer 0 pos))))
                     (setf pos 0))
                   (let ((p (punc c)))
                     (if p (see p)))))))))

(defun punc (c)
  (case c
    (#\. '|.|) (#\, '|,|) (#\; '|);|)
    (#\! '|!|) (#\? '|?|)))

;; (let ((prev '|.|))
;;   (defun see (symb)
;;     (let ((pair (assoc symb (gethash prev *words*))))
;;       (if (null pair)
;;           (push (cons symb 1) (gethash prev *words*))
;;           (incf (cdr pair))))
;;     (setf prev symb)))

;; (defun generate-text (n &optional (prev '|.|))
;;   (if (zerop n)
;;       (terpri)
;;       (let ((next (random-next prev)))
;;         (format t "~a " next)
;;         (generate-text (1- n) next))))

;; (defun random-next (prev)
;;   (let* ((choices (gethash prev *words*))
;;          (i (random (reduce #'+ choices :key #'cdr))))
;;     (dolist (pair choices)
;;       (if (minusp (decf i (cdr pair)))
;;           (return (car pair))))))



;;; Exercises

;; 1 Only if they are in different packages

;; 2 I would guess four times as much memory

;; 3 Symbols in packages could be dangerous if a symbol is interned by the same package it is named for, maybe that would cause a loop?

;; 4 Skipping this as I am already using packages

;; 5 Write a program to verify a quote was produced by henley
(defun is-henley? (quote)
  ;; loop for word in quote confirming the next word is in the word table as a possible word
  (let* ((quote (if (not (stringp quote))
                    (symbol-name quote)
                    quote))
         (tokens (tokenize quote)))
    (loop for word in tokens
          for next-word in (rest tokens)
            when (or (null (gethash word *words*))
                     (not (member next-word (gethash word *words*))))
              do (return-from is-henley? nil))
    t))

;; 6 Write a version of henley that can take a word and generate a sentence with that word in the middle.
(defun gen-sentence (word length)
  (declare (optimize (debug 3)))
  (if (not (is-henley? word))
      (error "Word must be in henley's dictionary.")
      (let* ((word-index (+ (floor length 4) (random (floor length 2))))
             (forwards (gen-text (- length word-index) word *words*))
             ;(backwards (gen-text word-index word *iwords*))
             (prev word))
        (loop for i from 0 below word-index
           collecting prev into backwards
              do (setf prev (random-next prev *iwords*))
           finally (format t "~{~a ~}~a" (reverse backwards) forwards)))))

(let ((prev '|.|))
  (defun see (symb)
    (let ((next-pair (assoc symb (gethash prev *words*)))
          (prev-pair (assoc prev (gethash symb *iwords*))))
      (if (null next-pair)
          (push (cons symb 1) (gethash prev *words*))
          (incf (cdr next-pair)))
      (if (null prev-pair)
          (push (cons prev 1) (gethash symb *iwords*))
          (incf (cdr prev-pair))))
    (setf prev symb)))

(defun random-next (prev &optional (dict *words*))
  (let* ((choices (gethash prev dict))
         (i (random (reduce #'+ choices :key #'cdr))))
    (dolist (pair choices)
      (if (minusp (decf i (cdr pair)))
          (return (car pair))))))

(defun generate-text (n &optional (prev '|.|))
  (format t "~a~%" (gen-text n prev)))

(defun gen-text (n &optional (prev '|.|) (dict *words*))
  (let ((prev prev)
        (output (make-array 0 :element-type 'character :fill-pointer 0)))
    (loop for i from 0 below n do
         (let ((next (random-next prev dict)))
           (format output "~a " (symbol-name next))
           (setf prev next)))
    output))
















