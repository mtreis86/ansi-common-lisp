;;;;Chapter 3
;;;;From ANSI Common Lisp

(in-package :ch3)


;;;Compression - when the same element occurs in a list
;;;multiple times in a row, substitute a list of that element
;;;and the count rather than the elemetents.
(defun compress (list)
 "Compress the list recursively using COMPR."
  (if (consp list)
      (compr (first list) 1 (rest list))
      list))

(defun compr (elt n list)
  "For each element in the list, confirm if it can be compressed by checking the next element. If it can, substitute a list of the element and the count."
  (if (null list)
      (list (n-elts elt n))
      (let ((next (first list)))
        (if (eql next elt)
            (compr elt (+ n 1) (rest list))
            (cons (n-elts elt n)
                  (compr next 1 (rest list)))))))

(defun n-elts (elt n)
  "Take in an element and a count and return a list of them. If the count is less than 1 return the element itself."
  (if (> n 1)
      (list n elt)
      elt))

(defun uncompress (list)
  "Walk through a list assuming it has been compressed. For every pair of (elt . n)- list of element . count in the list, replace that list with the number of elements represented by the count."
  (if (null list)
      nil
      (let ((elt (first list))
            (rest (uncompress (rest list))))
        (if (consp elt)
            (append (apply #'list-of elt) rest)
            (cons elt rest)))))

(defun list-of (n elt)
  "Expand elt into a list of n length."
  (if (zerop n)
      nil
      (cons elt (list-of (- n 1) elt))))


;;;Path searching.

;; (defun shortest-path (start end net)
;;   "Start a shortest-path search using bfs. Start is the beginning of the path, end the the end of the path, net is network to search within."
;;   (bfs end (list (list start)) net))

;; (defun bfs (end queue net)
;;   "Breadth-first search through the net, removing nodes from the queue if they don't satify the end."
;;   (format t "Queue:  ~a~%" queue)
;;   (if (null queue)
;;       nil
;;       (let ((path (first queue)))
;;         (format t " Path:  ~a~%" path)
;;         (let ((node (first path)))
;;           (format t "  Node: ~a~%" node)
;;           (if (eql node end)
;;               (reverse path)
;;               (bfs end (append (rest queue)
;;                                (new-paths path node net))
;;                    net))))))

(defun new-paths (path node net)
  "Generate new paths based on the network, storing them as assoc lists of nodes."
  (mapcar #'(lambda (n)
              (cons n path))
          (rest (assoc node net))))

;;;Exercises

;;1 Box notation
;;1a [a -]-> [b -]-> [| nil]
;;                    v
;;                    [c -]-> [d nil]

;;1b [a -]-> [| nil]
;;            v
;;            [b -]-> [| nil]
;;                     v
;;                     [c -]-> [| nil]
;;                              v
;;                              [d nil]

;;1c [| -]-> [d nil]
;;    v
;;    [| -]-> [c nil]
;;     v
;;     [a -]-> [b nil]

;;1d [a -]-> [| -]-> [d nil]
;;            v
;;            [b c]

;;2 new union
(defun new-union (&rest lists)
  "Return a list of the union of the input lists, keeping the inputs in order, add all of the second lists unions to the output before any of the third."
  (if (null (rest lists))
      lists
      (let ((result nil))
        (labels ((sub-union (list)
                   (loop for i in list do (setf result (adjoin i result)))))
          (loop for i in lists do (sub-union i)))
        (nreverse result))))

;;3 occurances
(defun occurances (list)
  "Take a list and return a list indicating the number of times each #'eql element occurs, sorted from most to least common."
  (let ((result nil))
    (loop for i in list do
      (if (assoc i result)
          (incf (rest (assoc i result)))
          (setf result (acons i 1 result))))
    (sort result #'(lambda (x y) (> (rest x) (rest y))))))

;;4 Member uses eql to test, and since the item being searched for is a list, and lists don't point to the same cons cell, it fails. Setting member to test for #'equal would work as it will ignore that.

;;5 Take a list and return the list of each element plus one
;;5recursive
(defun pos+r (list)
  (if (not (null (rest list)))
      (cons (+ 1 (first list)) (pos+r (rest list)))
      (list (+ 1 (first list)))))

;;5iterative
(defun pos+i (list)
  (loop for i in list collecting (+ i 1)))

;;5mapcar
(defun pos+m (list)
  (mapcar #'(lambda (x) (+ x 1)) list))

;;6 Swap car and cdr
(defun car-swap (list)
  (first (rest list)))

(defun cdr-swap (list)
  (first list))

(defun cons-swap (x y)
  (cons x y))

(defun list-swap (&rest lists)
  lists)

(defun length-swap (list)
  (length list))

(defun member-swap (item list)
  (member item list))

(assert (equal (car-swap '(a b)) 'b))
(assert (equal (cdr-swap '(a b)) 'a))
(assert (equal (cons-swap 'a 'b) '(a . b)))
(assert (equal (list-swap '(a b) '(c d)) '((a b) (c d))))
(assert (eq (length-swap '(a b c d)) 4))
(assert (equal (member-swap 'a '(a b c d)) '(a b c d)))

;;7 change compression code to be more efficient on consing

(defun compress-new (list)
 "Compress the list recursively using COMPR."
  (if (consp list)
      (compr-new (first list) 1 (rest list))
      list))

(defun compr-new (elt n list)
  "For each element in the list, confirm if it can be compressed by checking the next element. If it can, substitute a list of the element and the count."
  (if (null list)
      (list (n-elts-new elt n))
      (let ((next (first list)))
        (if (eql next elt)
            (compr-new elt (+ n 1) (rest list))
            (cons (n-elts-new elt n)
                  (compr-new next 1 (rest list)))))))

(defun n-elts-new (elt n)
  "Take in an element and a count and return a list of them. If the count is less than 1 return the element itself."
  (if (> n 1)
      (cons n elt)
      elt))

;;Original used 23,751 cycles to process '(1 1 1 0 1 0 0 0 0 1). New used 8,050.

;;8 Showdots
(defun showdots (list)
  "Print list in dot notation."
  (declare (optimize (debug 3)))
  (labels ((subdots (list)
             (cond ((atom list)
                    (format nil "~a" list))
                   ((null (rest list))
                    (format nil "(~a . NIL)" (subdots (first list))))
                   (t (format nil "(~a . ~a)" (subdots (first list))
                              (subdots (rest list)))))))
    (format t "~a" (subdots list))))

;;9 Modify path search to work with max as well as min

(defun paths-demo ()
  (let ((network '((a b c) (b c) (c d))))
    (shortest-path 'a 'd network)
    (longest-path 'a 'd network)))

(defun shortest-path (start end net)
  "Start a shortest-path search using bfs. Start is the beginning of the path, end the the end of the path, net is network to search within."
  (format t "Starting shortest path search...~%")
  (let ((path (bfs end (list (list start)) net 'min)))
    (format t "Shortest path: ~a~%~%" path)
    path))

(defun longest-path (start end net)
  "Start a shortest-path search using bfs. Start is the beginning of the path, end the the end of the path, net is network to search within."
  (format t "Starting longest path search~%")
  (let ((path (bfs end (list (list start)) net 'max)))
    (format t "Longest path ~a~%~%" path)
    path))

(defun bfs (end queue net &optional (test 'min))
  "Breadth-first search through the net, removing nodes from the queue if they don't satify the end."
  (declare (optimize (debug 3)))
  (format t "Queue:  ~a~%" queue)
  (if (null queue)
      nil
      (let ((path (first queue)))
        (format t " Path:  ~a~%" path)
        (let ((node (first path)))
          (format t "  Node: ~a~%" node)
          (if (eql node end)
              (reverse path)
              (bfs end (next-step queue path node net test)
                   net test))))))

(defun next-step (queue path node net test)
  (declare (optimize (debug 3)))
  (let ((new-path (append (rest queue) (new-paths path node net))))
    (cond ((equal test 'min) new-path)
          ((equal test 'max) (longest new-path))
          (t "Test not implemented"))))

(defun longest (list)
  (sort list #'(lambda (x y)
                 (> (length x) (length y)))))
