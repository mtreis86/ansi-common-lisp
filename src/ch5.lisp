;;;;Chapter 5 from ANSI Common Lisp

(in-package ch5)

(defvar month
  #(0 31 59 90 120 151 181 212 243 273 304 334 365))

(defvar yzero 2000)

(defun leap? (y)
  (and (zerop (mod y 4))
       (or (zerop (mod y 400))
           (not (zerop (mod y 100))))))

(defun date->num (d m y)
  (+ (- d 1) (month-num m y) (year-num y)))

(defun month-num (m y)
  (+ (svref month (- m 1))
     (if (and (> m 2)
              (leap? y))
         1
         0)))

(defun year-num (y)
  (let ((d 0))
    (if (>= y yzero)
        (dotimes (i (- y yzero) d)
          (incf d (year-days (+ yzero i))))
        (dotimes (i (- yzero y) (- d))
          (incf d (year-days (+ y i)))))))

(defun year-days (y)
  (if (leap? y)
      366
      365))

(defun num->date (n)
  (multiple-value-bind (y left) (num-year n)
    (multiple-value-bind (m d) (num-month left y)
      (values d m y))))

(defun num-year (n)
  (if (< n 0)
      (do* ((y (- yzero 1) (- y 1))
            (d (- (year-days y)) (- d (year-days y))))
           ((<= d n) (values y (- n d))))
      (do* ((y yzero (+ y 1))
            (prev 0 d)
            (d (year-days y) (+ d (year-days y))))
           ((> d n) (values y (- n prev))))))

(defun num-month (n y)
  (if (leap? y)
      (cond ((= n 59) (values 2 29))
            ((> n 59) (nmon (- n 1)))
            (t (nmon n)))
      (nmon n)))

(defun nmon (n)
  (let ((m (position n month :test #'<)))
    (values m (+ 1 (- n (svref month (- m 1)))))))

(defun date+ (d m y n)
  (num->date (+ (date->num d m y) n)))



;;; Exercises

;; 1 Translate into lambdas

;; (1a '(3 4)) => (3 . 3)
(defun 1a (y)
  ;; (let ((x (car y)))
  ;;   (cons x x)))
  (cons (funcall #'(lambda (y) (car y)) y)
        (funcall #'(lambda (y) (car y)) y)))

;; (1b '(3 4) 5) => (3 . 8)
(defun 1b (x z)
  ;; (let* ((w (car x))
  ;;        (y (+ w z)))
  ;;   (cons w y)))
  (cons (funcall #'(lambda (x) (car x)) x)
        (funcall #'(lambda (w z) (+ w z))
                 (funcall #'(lambda (x) (car x)) x)
                 z)))

;; 2 Rewrite mystery to use cond

(defun mystery (x y)
  ;; (if ((null y)
  ;;      nil
  ;;      (if (eql (car y) x)
  ;;          0
  ;;          (let ((z (mystery x (cdr y))))
  ;;            (and z (+ z 1)))))))
  (cond ((null y) nil)
        ((eql (car y) x) 0)
        (t (let ((z (mystery x (cdr y))))
             (and z (+ z 1))))))

;; 3 Write a function that returns the square of the input unless less than or equal to five, in which case it doesn't compute anything.

(defun square-over-five (x)
  (if (<= x 5)
      (RETURN-FROM square-over-five)
      (expt x 2)))

;; 4 Reqrite month-num to use case instead of svref

(defun my-month-num (m y)
  (+ (case m
       ((1) 31)  ((2) 59)  ((3) 90)  ((4)  120) ((5)  151) ((6)  181)
       ((7) 212) ((8) 243) ((9) 273) ((10) 304) ((11) 334) ((12) 365)
       (t 0))
     (if (and (> m 2)
              (leap? y))
         1
         0)))


;; 5 Define iteratively and recursively
;; Return a list of all objects that preceed object in vector

;; Iterative
(defun 5a (object vector)
  (let (list)
    (loop for index from 1 below (length vector)
          when (string= object (subseq vector index (+ index 1))) do
            (let ((new-entry (subseq vector (- index 1) index)))
              (when (not (member new-entry list :test #'string=))
                (push new-entry list))))
    list))

;; Recursive
(defun 5b (object vector)
  (let (list)
    (labels ((find-next (object vector)
               (let ((test (subseq vector 1 2))
                     (entry (subseq vector 0 1)))
                 (when (string= object test)
                   (when (not (member entry list :test #'string=))
                     (push entry list)))
                 (unless (< (array-dimension vector 0) 3)
                   (find-next object (subseq vector 1))))))
      (find-next object vector))
    list))


;; 6 Define iteratively and recursively
;; Take an object an a list and return a new list
;; in which the object appears in between all original elements.

;; Iterative
(defun 6a (object list)
  (loop for element in (butlast list)
        collecting element into newlist
        collecting object into newlist
        finally (RETURN (append newlist (last list)))))

;; Recursive
(defun 6b (object list)
  (let (newlist)
    (labels ((walk (object list)
               (setf newlist (append newlist (list (first list))))
               (when (not (null (rest list)))
                 (setf newlist (append newlist (list object)))
                 (walk object (rest list)))))
      (walk object list))
    newlist))


;; 7 Define using recursion, do, mapc and return
;; Take a list of numbers and return true only if the difference
;; between the numbers is 1

;; Recursive
(defun 7a (list)
  (cond ((null (rest list))
         t)
        ((= (abs (- (first list) (second list))) 1)
         (7a (rest list)))
        (t nil)))

;; Do
(defun 7b (list)
  (do ((next-list (rest list) (rest next-list))
       (x (first list) (first next-list))
       (y (second list) (second next-list)))
      ((null (rest next-list)) t)
    (when (not (= (abs (- x y)) 1)) (RETURN-FROM 7b nil))))

;; Mapc
(defun 7c (list)
  (let ((previous (first list)))
    (mapc #'(lambda (x)
              (when (not (= (abs (- x previous)) 1))
                (RETURN-FROM 7c nil))
              (setf previous x))
          (rest list)))
  t)


;; 8 Vector max min
;; Return the two values, max and min, for a vector. Find them recursively.
(defun vector-max-min (vector &optional max min)
  (if (zerop (array-dimension vector 0))
      (values max min)
      (let ((entry (aref vector 0))
            (next (subseq vector 1)))
        (cond ((and (null min) (null max))
               (vector-max-min next entry entry))
              ((or (null min) (< entry min))
               (vector-max-min next max entry))
              ((or (null max) (> entry max))
               (vector-max-min next entry min))
              (t (vector-max-min next max min))))))


;; 9 Rewrite bfs using catch/throw and return
;; Halt the search once a path is found

;; Catch/throw
(defun 9a (end queue net)
  "Breadth-first search through the net, removing nodes from the queue if they don't satify the end."
  (format t "Queue:  ~a~%" queue)
  (if (null queue)
      nil
      (let ((path (first queue)))
        (format t " Path:  ~a~%" path)
        (let ((node (first path)))
          (format t "  Node: ~a~%" node)
          (if (eql node end)
              (RETURN-FROM 9a (reverse path))
              (bfs end (append (rest queue)
                               (new-paths path node net))
                   net))))))

;; Return
(defun 9b (end queue net)
  "Breadth-first search through the net, removing nodes from the queue if they don't satify the end."
  (format t "Queue:  ~a~%" queue)
  (if (null queue)
      nil
      (let ((path (first queue)))
        (format t " Path:  ~a~%" path)
        (let ((node (first path)))
          (format t "  Node: ~a~%" node)
          (if (eql node end)
              (catch 'finished (9b-quit path))
              (bfs end (append (rest queue)
                               (new-paths path node net))
                   net))))))

(defun 9b-quit (path)
  (throw 'finished (reverse path)))



(defun bfs (end queue net)
  "Breadth-first search through the net, removing nodes from the queue if they don't satify the end."
  (format t "Queue:  ~a~%" queue)
  (if (null queue)
      nil
      (let ((path (first queue)))
        (format t " Path:  ~a~%" path)
        (let ((node (first path)))
          (format t "  Node: ~a~%" node)
          (if (eql node end)
              (reverse path)
              (bfs end (append (rest queue)
                               (new-paths path node net))
                   net))))))

(defun shortest-path (start end net)
  "Start a shortest-path search using bfs. Start is the beginning of the path, end the the end of the path, net is network to search within."
  (print 'bfs)
  (time (bfs end (list (list start)) net))
  (print 'return)
  (time (9a end (list (list start)) net))
  (print 'catch-throw)
  (time (9b end (list (list start)) net)))

(defun new-paths (path node net)
  "Generate new paths based on the network, storing them as assoc lists of nodes."
  (mapcar #'(lambda (n)
              (cons n path))
          (rest (assoc node net))))








