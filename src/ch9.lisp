(in-package :ch9)

;;;; Chapter 9 from ANSI Common Lisp

(defun sq (x) (* x x))

(defun mag (x y z)
  (sqrt (+ (sq x) (sq y) (sq z))))

(defun unit-vector (x y z)
  (let ((d (mag x y z)))
    (values (/ x d) (/ y d) (/ z d))))

(defstruct (point (:conc-name nil))
  x y z)

(defun distance (p1 p2)
  (mag (- (x p1) (x p2))
       (- (y p1) (y p2))
       (- (z p1) (z p2))))

(defun minroot (a b c)
  (if (zerop a)
      (/ (- c) b)
      (let ((disc (- (sq b) (* 4 a c))))
        (unless (minusp disc)
          (let ((discrt (sqrt disc)))
            (min (/ (+ (- b) discrt) (* 2 a))
                 (/ (- (- b) discrt) (* 2 a))))))))

(defstruct surface color)

(defparameter *world* nil)

(defparameter eye (make-point :x 0 :y 0 :z 200))

(defun tracer (pathname &optional (res 1))
  (with-open-file (p pathname :direction :output :if-exists :supersede)
    (format p "P2 ~A ~A 255" (* res 100) (* res 100))
    (let ((inc (/ res)))
      (do ((y -50 (+ y inc)))
          ((< (- 50 y) inc))
        (do ((x -50 (+ x inc)))
            ((< (- 50 x) inc))
          (print (color-at x y) p))))))

(defun color-at (x y)
  (multiple-value-bind (xr yr zr)
      (unit-vector (- x (x eye))
                   (- y (y eye))
                   (- 0 (z eye)))
    (round (* (sendray eye xr yr zr) 255))))

(defun sendray (pt xr yr zr)
  (multiple-value-bind (s int)
      (first-hit pt xr yr zr)
    (if s
        (* (lambert s int xr yr zr) (surface-color s))
        0)))

(defun first-hit (pt xr yr zr)
  (let (surface hit dist)
    (dolist (s *world*)
      (let ((h (intersect s pt xr yr zr)))
        (when h
          (let ((d (distance h pt)))
            (when (or (null dist) (< d dist))
              (setf surface s hit h dist d))))))
    (values surface hit)))

(defun lambert (s int xr yr zr)
  (multiple-value-bind (xn yn zn) (normal s int)
    (max 0 (+ (* xr xn) (* yr yn) (* zr zn)))))

(defstruct (sphere (:include surface))
  radius center)

(defun defsphere (x y z r c)
  (let ((s (make-sphere
            :radius r
            :center (make-point :x x :y y :z z)
            :color c)))
    (push s *world*)
    s))

(defun intersect (s pt xr yr zr)
  (funcall (typecase s (sphere #'sphere-intersect))
           s pt xr yr zr))

(defun sphere-intersect (s pt xr yr zr)
  (let* ((c (sphere-center s))
         (n (minroot (+ (sq xr) (sq yr) (sq zr))
                     (* 2 (+ (* (- (x pt) (x c)) xr)
                             (* (- (y pt) (y c)) yr)
                             (* (- (z pt) (z c)) zr)))
                     (+ (sq (- (x pt) (x c)))
                        (sq (- (y pt) (y c)))
                        (sq (- (z pt) (z c)))
                        (- (sq (sphere-radius s)))))))
    (if n
        (make-point :x (+ (x pt) (* n xr))
                    :y (+ (y pt) (* n yr))
                    :z (+ (z pt) (* n zr))))))

(defun normal (s pt)
  (funcall (typecase s (sphere #'sphere-normal))
           s pt))

(defun sphere-normal (s pt)
  (let ((c (sphere-center s)))
    (unit-vector (- (x c) (x pt))
                 (- (y c) (y pt))
                 (- (z c) (z pt)))))

(defun ray-test (&optional (res 1))
  (setf *world* nil)
  (defsphere 0 -300 -1200 200 .8)
  (defsphere -80 -150 -1200 200 .7)
  (defsphere 70 -100 -1200 200 .9)
  (do ((x -2 (1+ x)))
      ((> x 2))
    (do ((z 2 (1+ z)))
        ((> z 7))
      (defsphere (* x 200) 300 (* z -400) 40 .75)))
  (tracer (make-pathname :name "spheres.pgm") res))


;;; Exercises


;; 1 Non-decreasing reals

(defun non-decreasing-reals-p (list)
  "Return true if the list consists of numbers that don't decrease from one to the next."
  (cond ((null (rest list))
         t)
        ((<= (first list) (second list))
         (non-decreasing-reals-p (rest list)))
        (t nil)))


;; 2 Smallest number of coins

(defun fewest-coins (cents)
  "Take a integer number of cents and return the number of quarters, dimes, nickels and pennies that is the fewest count of coins to make up that value."
  (multiple-value-bind (quarters q-remainder)
      (floor cents 25)
    (multiple-value-bind (dimes d-remainder)
        (floor q-remainder 10)
      (multiple-value-bind (nickels pennies)
          (floor d-remainder 5)
        (values quarters dimes nickels pennies)))))


;; 3 Wigglies and Wobblies

;; Depends on how many live on the planet and the ratio of how many being of each type there are. If many live there and there are equal populations, their results are likely accurate.

;; After testing it is clear they are not accurate. Even at a population exceeding one million, ratios beyond 6:4 are likely given a normalized distribution of singing capability.

(defun wig-wob (wigs wobs)
  "Return two values, that total 10, and represent the number of winners of the contest where the number of participants compete but have similar capacity to win."
  ;; For fun I will use a normalized curve rather than all being equally capable.
  (let ((top-ten nil)
        (tenth 0))
    (defun new-singer (race)
      (let ((score (random-normal 10 1)))
        (when (is-better? score)
          (add-singer race score))))
    (defun is-better? (score)
      (if (> score tenth) t nil))
    (defun add-singer (race score)
      (when (>= (length top-ten) 10)
        (pop top-ten))
      (push (cons race score) top-ten)
      (setf top-ten (sort top-ten #'< :key #'rest))
      (setf tenth (rest (first top-ten))))
    (defun tally-votes ()
      (let ((wigs 0)
            (wobs 0))
        (loop for winner in top-ten do
             (if (equal (first winner) 'wig)
                 (incf wigs)
                 (incf wobs)))
        (format t "~%Wigs: ~A~%Wobs ~A~%" wigs wobs)))
    (defun contest ()
      (loop for order from 1 to wigs do
           (new-singer 'wig))
      (loop for order from 1 to wobs do
           (new-singer 'wob))
      (tally-votes)))
  (contest))


;; 4 Intersection of two line segments

(defun intersecting-line-segments (1ax 1ay 1bx 1by 2ax 2ay 2bx 2by)
  "Take four points representing two line segements, and if the segments intersect, return the intersection point, otherwise return nil."
  ;; Solve first for y, y = a x + b
  ;; Solve second for y, y = a x + b
  ;; Solve first = second for x, a x + b = a x + b
  ;; Solve both for y, if both ys are equal then the lines intersect at x,y
  ;; If that x,y is within the ranges of 1ax to 1bx, 1ay to 1by, 2ax to 2bx, and 2by to 2by, then the segments intersect
  (let* ((a1 (slope 1ax 1ay 1bx 1by))
         (a2 (slope 2ax 2ay 2bx 2by))
         (b1 (zeroc 1ax 1ay a1))
         (b2 (zeroc 2ax 2ay a2)))
    (multiple-value-bind (x y isect?)
        (intersecting-lines a1 b1 a2 b2)
      (if (and isect?
               (in-range? x 1ax 1bx)
               (in-range? x 2ax 2bx)
               (in-range? y 1ay 1by)
               (in-range? y 2ay 2by))
          (values (float x) (float y))
          nil))))

(defun slope (ax ay bx by)
  "What is the slope of the line that passes through these two points?"
  (/ (- ay by) (- ax bx))) ; slope is the diff in y over diff x

(defun zeroc (x y slope)
  "Where does a line with this slope that passes through this x an y intersect the x=0 line?"
  (let ((b y) ; Shift to x = 0
        (a slope))
    (+ (* a (- x)) b))) ; Solve for y at x = 0

(defun intersecting-lines (a1 b1 a2 b2)
  "Where do the lines in y = a * x + b form intersect?"
  (let* ((x (/ (- b2 b1) (- a1 a2))) ; Solve both for x at y = y
         (y1 (+ (* a1 x) b1)) ; Solve one for y
         (y2 (+ (* a2 x) b2)) ; Solve the other for y
         (isect? (= y1 y2))) ; Make sure y = y as parallel lines won't
    (values x y1 isect?)))

(defun in-range? (n a b)
  "Is the point n within the range a b?"
  ;; Since a or b could be min or max of the range, confirm n is not higher or lower than both a and b.
  (or (and (<= n a)
           (>= n b))
      (and (>= n a)
           (<= n b))))


;; 5 Funfind

(defun ffind (f min max epsilon)
  "Find the closest measure of i in f(i)=0, with i between min and max, and epsilon as the measurement to use."
  (loop
     with closest = min
     for i from min to max by epsilon
     when (< (abs (funcall f i)) (abs (funcall f closest)))
       do (setf closest i)
       finally (return closest)))


;; 6 Horners

(defun horners (x &rest args)
  "Evaluate the polynomial where the args are a list of a, b, c... in ax^2 + bx + c, with no limit to the number of coefficients."
  (loop
     with total = (+ (* (first args) x)
                     (if (second args)
                         (second args)
                         0))
     with rest = (rest (rest args))
     for coef in rest do
       (setf total (+ (* total x) coef))
     finally (return total)))


;; 7 SBCL seems to use 64 bits according to (length (format nil "~B" most-negative-fixnum)).


;; 8 SBCL seems to have two - long/double and short/single.
