;;;; Chapter 4 from ANSI Common Lisp

(in-package ch4)

;;; Binary search
(defun bin-search (obj vec)
  "Use finder to start a binary search in a sorted vec for the obj."
  (let ((len (length vec)))
    (and (not (zerop len))
         (finder obj vec 0 (- len 1)))))

(defun finder (obj vec start end)
  "Take an obj and a range within a vec and use binary search to see if the obj exists in that range."
  (format t "~a~%" (subseq vec start (+ end 1)))
  (let ((range (- end start)))
    (if (zerop range)
        (if (eql obj (aref vec start))
            obj
            nil)
        (let* ((mid (+ start (round (/ range 2))))
               (obj2 (aref vec mid)))
          (if (< obj obj2)
              (finder obj vec start (- mid 1))
              (if (> obj obj2)
                  (finder obj vec (+ mid 1) end)
                  obj))))))

;;; Date parsing

(defun tokens (string test start)
  "Break apart a string into tokens using a test function to separate them."
  (let ((pos1 (position-if test string :start start)))
    (if pos1
        (let ((pos2 (position-if #'(lambda (char)
                                     (not (funcall test char)))
                                 string :start pos1)))
          (cons (subseq string pos1 pos2)
                (if pos2
                    (tokens string test pos2)
                    nil)))
        nil)))

(defun constituent (char)
  "Is char both printable and not a space?"
  (and (graphic-char-p char)
       (not (char= char #\ ))))

(defun parse-date (string)
  "Take a date input in the form of day month year and parse them. (parse-date 16 Aug 1990) => (16 8 1990)."
  (let ((tokens (tokens string #'constituent 0)))
    (list (parse-integer (first tokens))
          (parse-month (second tokens))
          (parse-integer (third tokens)))))

(defparameter *month-names* #("jan" "feb" "mar" "apr" "may" "jun"
                              "jul" "aug" "sep" "oct" "nov" "dec"))

(defun parse-month (string)
  "Search month-names vec for the string, return the number of the month."
  (let ((pos (position string *month-names*
                       :test #'string-equal)))
    (if pos
        (+ pos 1)
        nil)))

;;; Binary search trees

(defstruct (node (:print-function
                  (lambda (n s d)
                    (declare (ignore d))
                    (format s "#<~a>" (node-elt n)))))
  elt (l nil) (r nil))


(defun bst-insert (obj bst <)
  "Add a new node to the binary tree, placing it based on the sorting function <."
  (if (null bst)
      (make-node :elt obj)
      (let ((elt (node-elt bst)))
        (if (eql obj elt)
            bst
            (if (funcall < obj elt)
                (make-node
                 :elt elt
                 :l (bst-insert obj (node-l bst) <)
                 :r (node-r bst))
                (make-node
                 :elt elt
                 :r (bst-insert obj (node-r bst) <)
                 :l (node-l bst)))))))

(defun bst-find (obj bst <)
  "Find an object in the binary tree, returning that object if found."
  (if (null bst)
      nil
      (let ((elt (node-elt bst)))
        (if (eql obj elt)
            bst
            (if (funcall < obj elt)
                (bst-find obj (node-l bst) <)
                (bst-find obj (node-r bst) <))))))

(defun bst-min (bst)
  "Find the minimum object in the tree."
  (and bst
       (or (bst-min (node-l bst))
           bst)))

(defun bst-max (bst)
  "Find the maximum object in the tree."
  (and bst
       (or (bst-max (node-r bst))
           bst)))

(defun bst-remove (obj bst <)
  "Remove an object from the tree. Return that tree after removal. Non-destructive."
  (if (null bst)
      nil
      (let ((elt (node-elt bst)))
        (if (eql obj elt)
            (percolate bst)
            (if (funcall < obj elt)
                (make-node
                 :elt elt
                 :l (bst-remove obj (node-l bst) <)
                 :r (node-r bst))
                (make-node
                 :elt elt
                 :l (node-l bst)
                 :r (bst-remove obj (node-r bst) <)))))))

(defun percolate (bst)
  "Helper function for bst-remove. Lets objects in the branches of a node shift position when the node is removed."
  (cond ((null (node-l bst))
         (if (null (node-r bst))
             nil
             (rperc bst)))
        ((null (node-r bst))
         (lperc bst))
        (t (if (zerop (random 2))
               (lperc bst)
               (rperc bst)))))

(defun rperc (bst)
  "Helper function for percolate. Creates a new node taking the right branch as the element."
  (make-node :elt (node-elt (node-r bst))
             :l (node-l bst)
             :r (percolate (node-r bst))))

(defun lperc (bst)
  "Helper function for percolate. Creates a new node taking the left branch as the element."
  (make-node :elt (node-elt (node-l bst))
             :l (percolate (node-l bst))
             :r (node-r bst)))

(defun bst-traverse (fn bst)
  "Taverse along the tree doing function at each node element."
  (when bst
    (bst-traverse fn (node-l bst))
    (funcall fn (node-elt bst))
    (bst-traverse fn (node-r bst))))



;;;Exercises

;; 1 Rotate a square array
;; I will write a rotate that works on rectangles too, why not.

(defun quarter-turn (array)
  "Rotate the array counter clockwise a quarter turn."
  (rotate array 'ccw))

(defun rotate (array direction)
  "Rotate an array in the direction, cw or ccw, a quarter turn. Nondestructive."
  (let* ((old-rows (array-dimension array 0))
         (old-cols (array-dimension array 1))
         (new-rows old-cols)
         (new-cols old-rows)
         (new (make-array (list new-rows new-cols))))
    (cond ((equal direction 'cw)
           (loop for new-row from 0 below new-rows
                 for old-col from (- old-cols 1) downto 0 do
                   (loop for new-col from 0 below new-cols
                         for old-row from 0 below old-rows do
                           (setf (aref new new-row new-col)
                                 (aref array old-row old-col)))))
          ((equal direction 'ccw)
           (loop for new-row from 0 below new-rows
                 for old-col from 0 below old-cols do
                   (loop for new-col from 0 below new-cols
                         for old-row from (- old-rows 1) downto 0 do
                           (setf (aref new new-row new-col)
                                 (aref array old-row old-col)))))
          (t (error "Direction not implemented")))
    new))

;; 2 Define copy-list and reverse using reduce

(defun my-copy-list (list)
  "Copy a list such that the new list is not eql to the old list."
  (reduce #'cons list :from-end t :initial-value nil))

(defun my-reverse (list)
  "Reverse a list non-destructively."
  (reduce #'(lambda (x y)
              (cons y x))
          list :initial-value nil))


;; 3 New structure for nodes; contain data and have upto three children.
;; Also define function to copy the tree, and another search the tree for some object being eql to one of the node's data.

(defstruct (new-node)
  elt (a nil) (b nil) (c nil))

(defun insert-mytree (tree object parent parent-slot)
  "Insert a new node into the tree with object as the element. If the tree doesn't exist, make it. If tree does exist, use the parent's parent-slot as the location for the new node."
  (if (null (new-node-p tree))
      (make-new-node :elt object)
      (let ((parent (find-mytree tree parent)))
        (cond ((equal parent-slot 'a)
               (setf (new-node-a parent) (make-new-node :elt object)))
              ((equal parent-slot 'b)
               (setf (new-node-b parent) (make-new-node :elt object)))
              ((equal parent-slot 'c)
               (setf (new-node-c parent) (make-new-node :elt object))))))
  tree)

(defun copy-mytree (tree)
  "Copy the tree such that new nodes are not eql to old ones."
  (let ((new-tree (make-new-node
                   :elt (copy-symbol (new-node-elt tree)))))
    (when (new-node-p (new-node-a tree))
      (setf (new-node-a new-tree) (copy-mytree (new-node-a tree))))
    (when (new-node-p (new-node-b tree))
      (setf (new-node-b new-tree) (copy-mytree (new-node-b tree))))
    (when (new-node-p (new-node-c tree))
      (setf (new-node-c new-tree) (copy-mytree (new-node-c tree))))
    new-tree))

(defun find-mytree (tree object)
  "Find an object in the tree, return it."
  (if (eql object (new-node-elt tree))
      tree
      (or (when (new-node-a tree) (find-mytree (new-node-a tree) object))
          (when (new-node-b tree) (find-mytree (new-node-b tree) object))
          (when (new-node-c tree) (find-mytree (new-node-c tree) object)))))


;; 4 Reverse the bst-traverse

(defun bst-reverse (bst)
  "Return a list of objects from the tree, ordered greatest to least."
  (bst-elements bst #'>))

(defun bst-elements (bst sort-fn)
  "Return a list of objects from the tree, ordered by the sort function."
  (when bst
    (sort (flatten (list (node-elt bst)
                         (when (node-l bst)
                           (bst-elements (node-l bst) sort-fn))
                         (when (node-r bst)
                           (bst-elements (node-r bst) sort-fn))))
          sort-fn)))


;; 5 New function, bst-adjoin. Only insert the node if one doesn't exist.

(defun bst-adjoin (obj bst <)
  "Add a new object to the tree if it doesn't exist."
  (if (bst-find obj bst <)
      bst
      (bst-insert obj bst <)))


;; 6 Make functions alist->hash and hash->alist

(defun alist->hash (alist)
  "Take in an alist list, and return a hash map. Assume input is ((key1 . value1) (key2 . value2) ...)."
  (when (null (first alist)) (error "Must be a list of lists."))
  (let ((hash (make-hash-table)))
    (loop for i in alist do
      (setf (gethash (first i) hash) (rest i)))
    hash))

(defun hash->alist (hash)
  "Take a hash table and return the equivalent alist list in the form ((key1 . value1) (key2 . value2) ...)"
  (let ((alist nil))
    (maphash #'(lambda (key value)
                 (setf alist (append alist (list (cons key value)))))
             hash)
    alist))
