;;;;Chapter 2
;;;;From ANSI Common Lisp

(in-package :ch2)

;;1a Evaluates 5 to 5, 1 to 1, 3 to 3, 7 to 7.
;;   Then passes 5 and 1 to the minus function which evaluates to 4.
;;   Then passes 3 and 7 to the plus function which evaluates to 10.
;;   Last it passes 4 and 10 to the plus function which evaluates to 14.

;;1b Evaluates 1 to 1, 2 to 2, and 3 to 3.
;;   Then passes 2 and 3 to the plus function which evaluates to 5.
;;   Then passes 1 and 5 to the list function which evaluates to (1 5)

;;1c Evaluates 1 to 1 then passes 1 to the listp function which returns nil.
;;   Since listp is nil, the if function skips the next expression,
;;   so it evaluates (+ 3 4) next by first evaluating 3 to 3, and 4 to 4.
;;   Last it passes the 3 and 4 to the plus function to return 7

;;1d Evaluates 3 to 3, 1 to 1, and 2 to 2.
;;   Then passes 3 to the listp function which returns nil.
;;   Passes that nil to the and function
;;   which then short circuits and returns nil.
;;   Passes 1 and 2 to plus which returns 3.
;;   Passes nil and plus to list function which returns (nil 3)

;;2 Use cons to produce (a b c) three ways
(cons 'a (cons 'b (cons 'c nil)))
(cons 'a (cons 'b '(c)))
(cons 'a '(b c))

;;3 Return the fourth element of a list
(defun my-fourth (list)
  (first (rest (rest (rest list)))))

;;4 Return the greater of two args
(defun greater (x y)
  (if (> x y)
      x
      y))

;;5a Takes in a list, x. Returns true if x contains an empty list at the start, or a series of empty lists, with an atom in the last rest.
(defun enigma (x)
  (and (not (null x))
       (or (null (first x))
           (enigma (rest x)))))

;;5b Finds the index of x in the list y.
(defun mystery (x y)
  (if (null y)
      nil
      (if (eql (first y) x)
          0
          (let ((z (mystery x (rest y))))
            (and z (+ z 1))))))

;;6a first

;;6b or

;;6c apply

;;7 Is x a list that contains a list as an element?
(defun has-list (list)
  (if (null list)
      nil
      (if (listp (first list))
          t
          (has-list (rest list)))))

;;;8  Solve both recursively and iteratively.

;;8a Take a positive int and print that many dots.
(defun r-dots (int)
  (if (< int 1)
      nil
      (progn
        (princ ".")
        (r-dots (- int 1)))))

(defun i-dots (int)
  (when (> int 0)
    (loop for i from 1 to int do (princ "."))))

;;8b Take a list and return the number of time a symbol occurs in it.
(defun r-count-symbols (list symbol)
  (when (listp list)
    (let ((count 0))
      (labels ((sub-count (list symbol)
                 (when (eq (first list) symbol)
                   (incf count))
                 (unless (null (rest list))
                   (sub-count (rest list) symbol))))
        (sub-count list symbol))
      count)))

(defun i-count-symbols (list symbol)
  (when (listp list)
    (loop for i in list counting (eq i symbol))))

;;9 Return the sum of all the non-nil elements in a list. a and b don't work.
;;  Explain why then write one that does.

;;9a Doesn't work because remove is non-destructive so the apply function is
;;   operating on the full list.
(defun summit-a-bad (list)
  (remove nil list)
  (apply #'+ List))

(Defun summit-a (list)
  (apply #'+ (remove nil list)))

;;9b Doesn't work because there is no way to end the recusion.
(defun summit-b-bad (list)
  (let ((x (first list)))
    (if (null x)
        (summit-b-bad (rest list))
        (+ x (summit-b-bad (rest list))))))

(defun summit-b (list)
  (let ((x 0))
    (labels ((sub-sum (list)
               (unless (null (rest list))
                 (when (not (null (first list)))
                   (incf x (first list)))
                 (sub-sum (rest list)))))
      (sub-sum list))
    x))
