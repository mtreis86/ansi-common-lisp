;;;; Chapter 10 from ANSI Common Lisp

(in-package :ch10)

(defmacro while (test &rest body)
  `(do ()
       ((not ,test))
     ,@body))

(defun quicksort (vec l r)
  (let ((i l)
        (j r)
        (p (svref vec (round (+ l r) 2))))
    (while (<= i j)
      (while (< (svref vec i) p) (incf j))
      (while (> (svref vec j) p) (decf j))
      (when (<= i j)
        (rotatef (svref vec i) (svref vec j))
        (incf i)
        (decf j)))
    (if (> (- j l) 1) (quicksort vec l j))
    (if (> (- r i) 1) (quicksort vec i r))))

(defmacro for (var start stop &body body)
  (let ((gstop (gensym)))
    `(do ((,var ,start (1+ ,var))
          (,gstop ,stop))
         ((> ,var ,gstop))
       ,@body)))

(defmacro in (obj &rest choices)
  (let ((insym (gensym)))
    `(let ((,insym ,obj))
       (or ,@(mapcar #'(lambda (c) `(eql ,insym ,c))
                     choices)))))

(defmacro random-choice (&rest exprs)
  `(case (random ,(length exprs))
     ,@(let ((key -1))
         (mapcar #'(lambda (expr) `(,(incf key) ,exprs))
                 exprs))))

(defmacro avg (&rest args)
  `(/ (+ ,@args) ,(length args)))

(defmacro with-gensyms (syms &body body)
  `(let ,(mapcar #'(lambda (s) `(,s (gensym)))
                 syms)
     ,@body))

(defmacro aif (test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))


;;; Exercises

;; 1 Backquote
(defun test-1 ()
  (let ((x 'a) (y 'b) (z '(c d)))
    (print `(,z ,x z))
    (print `(x ,y ,@z))
    (print `((,@z ,x) z))
    nil))

;; 2 If in terms of cond

(defmacro ifcm (this then else)
  `(cond ((,@this)
          (,@then))
         (t (,@else))))

;; 3 nth expr
(defmacro nth-expr (n &rest exprs)
  "Return the value of the nth expression."
  (loop for expr in exprs
     for index from 1 below n
       finally (return expr)))

;; 4 Define ntimes as a recursive macro
(defmacro ntimes (n &rest body)
  `(when (> ,n 0)
         ,@body
         (ntimes (- ,n 1) ,@body)))

;; 5 n-of
(defmacro n-of (n expr)
  "Return list of calling expr n times."
  (let ((i (gensym)))
    `(loop for ,i from 0 below ,n
        collecting ,expr into list
          finally (return list))))

;; 6 Revert
(defmacro revert-after-eval (vars &rest body)
  "Take a list of vars, eval the body, then restore the vars to where they were."
  `(let ,(mapcar #'(lambda (var) `(,var ,var))
                 vars)
     ,@body))


;; 7 This can be written as a function instead of a macro.

;; 8 Macro that doubles
(defmacro double (var)
  `(setf ,var (* ,var 2)))
